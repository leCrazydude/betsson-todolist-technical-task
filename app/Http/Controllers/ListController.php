<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\TodoList;
use App\Models\Label;

use Auth;

class ListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTasks()
    {
        $todoList = TodoList::with('label')->select(['todo_list.*', 'labels.label_title'])->leftJoin('labels', 'todo_list.label_id', '=', 'labels.id')->where('complete', '0')->where('todo_list.user_id', Auth::user()->id)->get();

        foreach ($todoList as $key => $task) {
            $todoList[$key]['task_from'] = !empty($task['task_from']) ? date('Y-m-d H:i', strtotime($task['task_from'])) : null;
            $todoList[$key]['task_to'] = !empty($task['task_to']) ? date('Y-m-d H:i', strtotime($task['task_to'])) : null;
        }

        return response()->json([
            "todoList" => $todoList
        ]);
    }

    public function getLabels()
    {
        $labels = Label::where('user_id', Auth::user()->id)->get();

        return response()->json([
            "labels" => $labels
        ]);
    }

    public function add()
    {
        if (!empty(request('task_from')) && !empty(request('task_to'))) {
            $overlappingCount = $this->checkOverlappingDates(request('task_from'), request('task_to'));
            
            if (!empty($overlappingCount)) {
                return response()->json([
                    'message' => 'Task exists in dates selected!',
                    'status' => 0
                ], 500);
            }
        }
        
        $label = null;
        if (!empty(request('label'))) {
            $label = Label::where('label_title', request('label'))->where('user_id', Auth::user()->id)->get()->first();
    
            if (empty($label)) {
                $label = Label::create([
                    'label_title' => request('label'),
                    'user_id' => Auth::user()->id
                ]);
            }
        }

        $newTask = TodoList::create([
            'task_title' => request('task_title'),
            'task_description' => request('task_description'),
            'user_id' => Auth::user()->id,
            'label_id' => !empty($label) ? $label->id : null,
            'priority' => request('priority'),
            'task_from' => request('task_from'),
            'task_to' => request('task_to'),
            'rec_status' => 1,
        ]);

        return response()->json([
            'data' => $newTask,
            'message' => 'Successfully added new task!',
            'status' => 1
        ]);
    }

    public function update()
    {
        if (empty(request('id'))) {
            return response()->json([
                'message' => 'Unable to update task!',
                'status' => 0
            ], 500);
        }

        if (!empty(request('task')['task_from']) && !empty(request('task')['task_to'])) {
            $overlappingCount = $this->checkOverlappingDates(request('task')['task_from'], request('task')['task_to'], request('id'));
            
            if (!empty($overlappingCount)) {
                return response()->json([
                    'message' => 'Task exists in dates selected!',
                    'status' => 0
                ], 500);
            }
        }

        $label = null;
        if (!empty(request('task')['label'])) {
            $label = Label::where('label_title', request('task')['label'])->where('user_id', Auth::user()->id)->get()->first();
    
            if (empty($label)) {
                $label = Label::create([
                    'label_title' => request('task')['label'],
                    'user_id' => Auth::user()->id
                ]);
            }
        }

        $updateTask = TodoList::where('id', request('id'))->where('user_id', Auth::user()->id)
                                ->update([
                                    'task_title' => request('task')['task_title'],
                                    'task_description' => request('task')['task_description'],
                                    'label_id' => !empty($label) ? $label->id : null,
                                    'priority' => request('task')['priority'],
                                    'task_from' => request('task')['task_from'],
                                    'task_to' => request('task')['task_to']
                                ]);
        
        $this->removeUnusedLabels();
        
        return response()->json([
            'data' => $updateTask,
            'message' => 'Successfully updated task!',
            'status' => 1
        ]);
    }

    public function delete()
    {
        TodoList::where('id', request('id'))->where('user_id', Auth::user()->id)->delete();

        $this->removeUnusedLabels();

        return response()->json([
            'message' => 'Successfully deleted task!',
            'status' => 1
        ]);
    }

    public function complete()
    {
        if (empty(request('id'))) {
            return response()->json([
                'message' => 'Unable to complete task!',
                'status' => 0
            ], 500);
        }

        $completeTask = TodoList::where('id', request('id'))->where('user_id', Auth::user()->id)
                                ->update([
                                    'complete' => 1
                                ]);
        
        $this->removeUnusedLabels();
        
        return response()->json([
            'data' => $completeTask,
            'message' => 'Successfully completed task!',
            'status' => 1
        ]);
    }

    private function removeUnusedLabels()
    {
        $unusedLabels = Label::with('TodoList')->select(['labels.*', 'todo_list.label_id'])
                                               ->leftJoin('todo_list', 'labels.id', 'todo_list.label_id')
                                               ->whereNull('todo_list.label_id')
                                               ->orWhere('todo_list.complete', '1')
                                               ->Where('labels.user_id', Auth::user()->id)
                                               ->get();
        foreach ($unusedLabels as $key => $unusedLabel) {
            Label::where('id', $unusedLabel->id)->where('user_id', Auth::user()->id)->delete();
        }
    }

    private function checkOverlappingDates($startTime, $endTime, $updateId = null)
    {
        return TodoList::where(function ($query) use ($startTime, $endTime, $updateId) {
            $query->where(function ($query) use ($startTime, $endTime, $updateId) {
                $query->where('task_from', '<=', $startTime)
                      ->where('task_to', '>=', $startTime)
                      ->where('id', '!=', $updateId);
            })
            ->orWhere(function ($query) use ($startTime, $endTime, $updateId) {
                $query->where('task_from', '<=', $endTime)
                      ->where('task_to', '>=', $endTime)
                      ->where('id', '!=', $updateId);
            })
            ->orWhere(function ($query) use ($startTime, $endTime, $updateId) {
                $query->where('task_from', '>=', $startTime)
                      ->where('task_to', '<=', $endTime)
                      ->where('id', '!=', $updateId);
            })
            ->orWhere(function ($query) use ($startTime, $endTime, $updateId) {
                $query->where('task_from', '<=', $startTime)
                      ->where('task_to', '>=', $endTime)
                      ->where('id', '!=', $updateId);
            });
        })->count();
    }
}
