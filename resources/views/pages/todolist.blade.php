@extends('layouts.default')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <todolist-component></todolist-component>
        </div>
    </div>

@endsection