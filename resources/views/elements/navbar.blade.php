<div class="navbar">
   <div class="navbar-inner">
        <a href="/">
            <img id="logo" src="{{ URL::to('/') }}/media/logo.png" alt="Betsson Logo">
            <span id="logo-subtitle">Technical Task</span>
        </a>
   </div>
</div>