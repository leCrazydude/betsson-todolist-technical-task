import './bootstrap';
import '../sass/app.scss'

import Router from '@/router'
import store from '@/store'

import { createApp } from 'vue/dist/vue.esm-bundler';

const app = createApp({})

import TodoListComponent from './components/TodoListComponent.vue';
app.component('todolist-component', TodoListComponent);

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)
app.component('font-awesome-icon', FontAwesomeIcon);

app.use(Router)
app.use(store)
app.mount('#app')