<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('todo_list', function (Blueprint $table) {
            $table->id();
            $table->string('task_title');
            $table->string('task_description')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('label_id')->nullable();
            $table->string('priority')->nullable();
            $table->dateTime('task_from')->nullable();
            $table->dateTime('task_to')->nullable();
            $table->integer('complete')->default(0);
            $table->timestamps();
            $table->integer('rec_status')->default(1);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('label_id')->references('id')->on('labels')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('todo_list');
    }
};
