<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ListController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); })->where('any', '.*');
Route::get('/todolist', function () { return view('welcome'); })->where('any', '.*');

Route::get('/login', function () {
    return view('welcome');
})->where('any', '.*');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/todolist/get-tasks', [ListController::class, 'getTasks']);
Route::get('/todolist/get-labels', [ListController::class, 'getLabels']);
Route::post('/todolist/add', [ListController::class, 'add']);
Route::put('/todolist/update/{id}', [ListController::class, 'update']);
Route::put('/todolist/complete/{id}', [ListController::class, 'complete']);
Route::delete('/todolist/delete/{id}', [ListController::class, 'delete']);